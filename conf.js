// An example configuration file. 
exports.config = {
    framwork: 'jasmine',
    // The address of a running selenium server. 
    //seleniumAddress: 'http://localhost:4444/wd/hub',
    specs: ['signup.js','signup3.js','signin.js','signintest.js'], 
 jasmineNodeOpts: {
    defaultTimeoutInterval: 360000
  },
    // Capabilities to be passed to the webdriver instance. 
    capabilities: {
        'browserName': 'chrome'
    }    
};